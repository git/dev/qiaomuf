# Copyright 1999-2012 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=4

if [[ $PV == 9999 ]]; then
	EGIT_REPO_URI="git://github.com/qiaomuf/libbash.git"
	inherit git-2
	SRC_URI=""
else
	SRC_URI="http://dev.gentoo.org/~qiaomuf/libbash-0.1.tar.gz"
	KEYWORDS="~amd64 ~x86"
fi

inherit java-pkg-opt-2

DESCRIPTION="A C++ library for bash"
HOMEPAGE="http://www.gentoo.org/proj/en/libbash/index.xml"
LICENSE="GPL-2"

SLOT="0"
IUSE="doc test"

DEPEND="
	dev-java/antlr:3[gunit]
	>=dev-libs/antlr-c-3.3_pre20110403
	>=dev-libs/boost-1.43.0
	<sys-apps/paludis-0.62.0
	>=sys-devel/gcc-4.5.1
	doc? (	app-doc/doxygen
			dev-texlive/texlive-latex )
	test? ( >=virtual/jdk-1.4
			dev-cpp/gtest )"

RDEPEND=">=dev-libs/antlr-c-3.3_pre20110403"

if [[ $PV == 9999 ]]; then
	src_prepare() {
		./autogen.sh
	}
fi
